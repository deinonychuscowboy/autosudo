AUTOSUDO
========

Autosudo is a little utility that handles requesting sudo permissions when they appear to be necessary.
It is fully customizable and does not handle any sensitive information itself. It only decides whether invoking sudo is necessary for a given command.

There exists a disconnect between modern linux permissions on the command line vs. within a gui environment like GNOME or KDE. When executing a privileged program like gparted from within GNOME, you are presented with a pkexec dialog to enter your sudo password.
Other applications, such as parts of the KDE settings subsystem, execute as a normal user, but request elevated permissions upon modifying/saving values. This is all transparent and automatic to the user, who need use no special behavior to execute these applications.
On the command line, however, if you have the habit that 'nano' is the command used to edit files, and you wish to edit /etc/hosts, you might naturally type `nano /etc/hosts`. This, of course, is not the correct command if you wish to _edit_ /etc/hosts, because
you require permission to do so. Unlike in a gui environment, the procedure for editing a protected file or running a protected program is different from that of editing other files or running other programs, and requires forethought about whether the action you're
performing requires a special means of execution.

This is not a unique problem (the Windows shell behaves similarly, as do many linux gui-based text editors when running as the user but trying to edit a file owned by root), but it does become particularly prominent on the linux command line, thanks to linux's
heavy reliance on config files and command-line applications, some of which have no gui alternative, and some of which are easier to use than their gui equivalents for certain tasks. If the gui is able to determine when a user needs special permission (even if
gksu is used in the background, the user experience is that of an automatic determination), the command line ought to be able to do so as well.

How to use Autosudo
-------------------

Autosudo is a tiny bash script that can process your command line entries. When it is called with a specific command, it checks two things: what permissions you have, and what permissions you need. The manner in which it determines these things is entirely
configurable per program (i.e. if you want to check the group permissions, you are free to do that; the examples only check ownership).

If the permissions you need do not match the permissions you have, autosudo executes your command with sudo. If they do match, it executes your command normally.

Installing
----------

Drop the autosudo script somewhere in your path, such as /usr/local/bin/autosudo. Merge the .config folder with the same in your home directory. Autosudo comes with a couple example configurations to demonstrate how to write the configuration.

Within the permissions/, tests/, and (optionally) aliases/ folders, you should supply executable scripts (in any language you want) which have names matching the programs you want to execute. E.g. to auto-sudo nano, you want a permissions/nano script, a tests/nano script, and (optionally) an aliases/nano script. Autosudo will call these scripts, and expect the following output:
- for permissions/ , the script should output the permissions you have
- for tests/ , the script should output the permissions you will need to execute the supplied arguments (passed to tests/script, e.g. the filename you intend to edit will be passed to tests/nano)
- for aliases/ , if you desire, you can pass elements to add to the alias autosudo will generate for you, such as command line args

To enable autosudo for a specific command, you also need to redirect calls to that command to autosudo. Autosudo can generate these aliases for you, assuming your config files are well-formed and executable, if you run `autosudo --update-autosudo`. Then, just source `. "$HOME/.autosudorc"` in your .bashrc or similar. Or, you can manually add aliases to your shell's config, e.g.:

```
alias nano='autosudo nano'
```

Remember to exit your current terminal session (or reload config) before trying to use the new alias.
